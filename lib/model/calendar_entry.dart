class CalendarEntry {
  int id;
  String title;
  String description;
  String start;
  String end;
  String recurrenceEnd;
  bool cancelled;
  int event;
  int frequency;
  int calendar;
  List<Participants> participants;
  String originalStart;
  String originalEnd;
  Creator creator;

  CalendarEntry(
      {this.id,
      this.title,
      this.description,
      this.start,
      this.end,
      this.recurrenceEnd,
      this.cancelled,
      this.event,
      this.frequency,
      this.calendar,
      this.participants,
      this.originalStart,
      this.originalEnd,
      this.creator});

  CalendarEntry.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    start = json['start'];
    end = json['end'];
    recurrenceEnd = json['recurrence_end'];
    cancelled = json['cancelled'];
    event = json['event'];
    frequency = json['frequency'];
    calendar = json['calendar'];
    if (json['participants'] != null) {
      participants = new List<Participants>();
      json['participants'].forEach((v) {
        participants.add(new Participants.fromJson(v));
      });
    }
    originalStart = json['original_start'];
    originalEnd = json['original_end'];
    creator =
        json['creator'] != null ? new Creator.fromJson(json['creator']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    data['start'] = this.start;
    data['end'] = this.end;
    data['recurrence_end'] = this.recurrenceEnd;
    data['cancelled'] = this.cancelled;
    data['event'] = this.event;
    data['frequency'] = this.frequency;
    data['calendar'] = this.calendar;
    if (this.participants != null) {
      data['participants'] = this.participants.map((v) => v.toJson()).toList();
    }
    data['original_start'] = this.originalStart;
    data['original_end'] = this.originalEnd;
    if (this.creator != null) {
      data['creator'] = this.creator.toJson();
    }
    return data;
  }
}

class Creator {
  int id;
  String firstName;
  String lastName;
  String email;

  Creator({this.id, this.firstName, this.lastName, this.email});

  Creator.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    return data;
  }
}

class Participants {
  int id;
  String firstName;
  String lastName;
  String email;

  Participants({this.id, this.firstName, this.lastName, this.email});

  Participants.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    return data;
  }
}
