import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// import '../../models/calendar_entry.dart';
// calendar_event_list.dart';
import 'package:mobile_new/model/calendar_entry.dart';

class CalendarEventList extends StatelessWidget {
  final List<CalendarEntry> entries;

  CalendarEventList({this.entries});

  String getFullname(String firstName, String lastName) =>
      '$firstName $lastName';

  String getInitials(String firstName, String lastName) =>
      '${firstName[0]}${lastName[0]}';

  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      child: ListView.builder(
        itemCount: entries.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 16, right: 36),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              DateFormat.Hm().format(
                                  DateTime.parse('${entries[index].start}')),
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                              ),
                            ),
                            Text(
                              '1h',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: 4,
                        left: 62,
                        child: Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                            color: Colors.red[900],
                            borderRadius: BorderRadius.circular(100.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '${entries[index].title}',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          for (int i = 0;
                              i < entries[index].participants.length;
                              i += 1)
                            Container(
                              width: 28,
                              height: 28,
                              margin: EdgeInsets.only(right: 4),
                              child: CircleAvatar(
                                backgroundColor:
                                    entries[index].participants[i].id ==
                                            entries[index].creator.id
                                        ? Colors.purple[300]
                                        : Colors.cyan[400],
                                foregroundColor: Colors.white,
                                child: Text(
                                  getInitials(
                                    entries[index].participants[i].firstName,
                                    entries[index].participants[i].lastName,
                                  ),
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                            ),
                        ],
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.location_on,
                            size: 16,
                            color: Colors.grey,
                          ),
                          Text(
                            '123 Main Street',
                            style: TextStyle(color: Colors.grey, fontSize: 12),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Divider(
                thickness: 0.6,
              ),
            ],
          );
        },
      ),
    );
  }
}
