import 'package:flutter/material.dart';
// import 'package:mobile_new/calendar/calendar_event_list.dart';
import 'package:mobile_new/calendar/copy_start.dart';
import 'package:mobile_new/model/calendar_entry.dart';
import 'package:mobile_new/provider/api/calendar_api_provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:mobile_new/layout/navigation_drawer.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class CalendarManager extends StatefulWidget {
  @override
  _CalendarManagerState createState() => _CalendarManagerState();
}

class _CalendarManagerState extends State<CalendarManager> {
  Map<DateTime, List> _events;

  List<DateTime> _visibleDaysList;

  DateTime _selectedDate = DateTime.now();
  List<CalendarEntry> _selectedEvents = [];

  Completer<void> _refreshCompleter;

  final _calendarController = CalendarController();
  CalendarApiProvider _apiProvider = CalendarApiProvider();
  @override
  void initState() {
    _refreshCompleter = Completer<void>();
    DateTime today = DateTime.now();
    DateTime monthStart = DateTime(today.year, today.month, 1);
    int numMonthDays = DateTime(today.year, (today.month + 1) % 12, 0).day;
    DateTime monthEnd = DateTime(today.year, today.month, numMonthDays);
    _apiProvider.getCalendarOccurrences(monthStart, monthEnd).then((events) {
      setState(() {
        _events = events;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(day, events) {
    print('DAY $day EVENTS $events');

    setState(() {
      _selectedDate = day;
      _selectedEvents = List<CalendarEntry>.from(events);
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    // print(_calendarController._getFirstDay(false));
    // print(_calendarController.visibleDays);
    // print('$first - $last -');
    _apiProvider.getCalendarOccurrences(first, last).then((events) {
      setState(() {
        _visibleDaysList = _calendarController.visibleDays;
        _selectedDate = first;
        // _calendarController.setFocusedDay(first);

        _events = events;
        // _selectedEvents = events;
        // events = _selectedEvents;
      });
    });
    print('CALLBACK: _onVisibleDaysChanged');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: NavigationDrawer(),
      appBar: AppBar(
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 28,
                  height: 28,
                  margin: EdgeInsets.only(right: 4),
                  child: CircleAvatar(
                    backgroundColor: Colors.green[400],
                    foregroundColor: Colors.white,
                    child: Text(
                      'LH'.toUpperCase(),
                      // style: TextStyle(fontWeight: FontWeight.w400),

                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                )
                //  CircleAvatar(
                //   maxRadius: 16,
                //   backgroundColor: Colors.red[400],
                //   foregroundColor: Colors.white,
                //   child: Center(
                //     child: IconButton(
                //       icon: Text(
                //         'd'.toUpperCase(),
                //         style: TextStyle(fontWeight: FontWeight.w400),
                //       ),
                //       onPressed: () {
                //         // Scaffold.of(context).openDrawer();
                //       },
                //       tooltip:
                //           MaterialLocalizations.of(context).openAppDrawerTooltip,
                //     ),
                //   ),
                // ),
                );
          },
        ),
        titleSpacing: 0.0,
        title: Text('My Schedule'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            tooltip: 'Add new entry',
            onPressed: () => {},
          ),
          IconButton(
            icon: Icon(Icons.more_vert),
            tooltip: 'Add new entry',
            onPressed: () => {},
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () {
          _apiProvider
              .getCalendarOccurrences(_calendarController.visibleDays.first,
                  _calendarController.visibleDays.last)
              .then((events) {
            setState(() {
              _events = events;
              _refreshCompleter?.complete();
              _refreshCompleter = Completer();
            });
          });
          return _refreshCompleter.future;
        },
        child: Container(
          child: Column(
            children: <Widget>[
              TableCalendar(
                  initialCalendarFormat: CalendarFormat.month,
                  calendarController: _calendarController,
                  calendarStyle: CalendarStyle(
                    selectedColor: Colors.blue,
                    todayColor: Colors.blue[200],
                    markersColor: Colors.grey[800],
                    outsideDaysVisible: false,
                    weekendStyle: TextStyle().copyWith(color: Colors.grey[800]),
                    highlightToday: true,
                    renderSelectedFirst: true,
                    holidayStyle: TextStyle().copyWith(color: Colors.grey[800]),
                  ),
                  daysOfWeekStyle: DaysOfWeekStyle(
                    weekendStyle: TextStyle().copyWith(color: Colors.grey[600]),
                    dowTextBuilder: (date, locale) =>
                        DateFormat.E(locale).format(date)[0],
                  ),
                  headerStyle: HeaderStyle(
                    centerHeaderTitle: true,
                    formatButtonVisible: false,
                    formatButtonTextStyle: TextStyle()
                        .copyWith(color: Colors.white, fontSize: 15.0),
                    formatButtonDecoration: BoxDecoration(
                      color: Colors.blue[400],
                      borderRadius: BorderRadius.circular(16.0),
                    ),
                    titleTextStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32,
                      color: Colors.white,
                    ),
                    decoration: BoxDecoration(color: Colors.blue),
                    leftChevronIcon:
                        Icon(Icons.chevron_left, color: Colors.white),
                    rightChevronIcon:
                        Icon(Icons.chevron_right, color: Colors.white),
                  ),
                  events: _events,
                  onDaySelected: _onDaySelected,
                  onVisibleDaysChanged: _onVisibleDaysChanged,
                  builders: CalendarBuilders(
                    markersBuilder: (context, date, events, holidays) {
                      final children = <Widget>[];
                      if (events.isNotEmpty) {
                        children.add(
                          Positioned(
                            right: 1,
                            bottom: 1,
                            child: _buildEventsMarker(date, events),
                          ),
                        );
                      }
                      return children;
                    },
                  )),
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(color: Colors.blue[50]),
                child: Text(
                  // '$_selectedDate',
                  _calendarController.isToday(_selectedDate)
                      ? 'Today ' + DateFormat.MMMMEEEEd().format(_selectedDate)
                      : DateFormat.MMMMEEEEd().format(_selectedDate),
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                    color: _calendarController.isToday(_selectedDate)
                        ? Colors.blueGrey
                        : Colors.blueGrey,
                  ),
                ),
              ),
              Expanded(
                child: CalendarEventList(entries: _selectedEvents),
              ),
              // Expanded(
              //   // child: RefreshIndicator(
              //   //   onRefresh: () {
              //       // _apiProvider.getCalendarOccurrences(_calendarController.visibleDays.first, _calendarController.visibleDays.last).then((events) {
              //       //   setState(() {
              //       //     _events = events;
              //       //   });
              //       // });
              //       // return null;
              //     // },
              // //     _refreshCompleter?.complete()
              // // _refreshCompleter = Completer()

              //   child:
              //    Column(
              //     children: <Widget>[
              //         CalendarEventList(entries: _selectedEvents),

              //     ],
              //   ),
              //   // ),
              // )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: _calendarController.isSelected(date)
            ? _calendarController.isToday(date)
                ? Colors.grey[800]
                : Colors.grey[700]
            : Colors.blue[400],
      ),
      width: 16.0,
      height: 16.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }
}
