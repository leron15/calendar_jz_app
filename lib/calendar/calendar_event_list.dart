import 'package:flutter/material.dart';
import 'package:mobile_new/model/calendar_entry.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:mobile_new/calendar/calendar_event_list.dart';
import 'package:mobile_new/model/calendar_entry.dart';
import 'package:mobile_new/provider/api/calendar_api_provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:mobile_new/layout/navigation_drawer.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class CalendarEventList extends StatelessWidget {
  final List<CalendarEntry> entries;

  const CalendarEventList({Key key, @required this.entries}) : super(key: key);

  String getFullname(String firstName, String lastName) =>
      '$firstName $lastName';

  String getInitials(String firstName, String lastName) =>
      '${firstName[0]}${lastName[0]}';

  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  // Widget build(BuildContext context) {
  //   return Container(
  //     margin: EdgeInsets.symmetric(vertical: 16),
  //     child: entries.isNotEmpty
  //         ? ListView.builder(
  //             shrinkWrap: true,
  //             itemCount: entries.length,
  //             itemBuilder: (context, index) {
  //               return ListTile(
  //                 dense: entries[index].participants.isEmpty ? true : false,
  //                 isThreeLine: false,
  //                 leading: Stack(
  //                   children: <Widget>[
  //                     Row(
  //                       mainAxisSize: MainAxisSize.min,
  //                       children: <Widget>[
  //                         Container(
  //                           margin: EdgeInsets.only(right: 10),
  //                           child: Column(
  //                             mainAxisAlignment: MainAxisAlignment.start,
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             children: <Widget>[
  //                               Text(
  //                                 DateFormat.jm().format(
  //                                     DateTime.parse(entries[index].start)),
  //                                 style: TextStyle(
  //                                   color: Colors.black,
  //                                   // fontWeight: FontWeight.w300,
  //                                   // fontSize: 16.0,
  //                                 ),
  //                                 textAlign: TextAlign.center,
  //                               ),
  //                               Text(
  //                                 DateTime.parse(entries[index].end)
  //                                         .difference(DateTime.parse(
  //                                             entries[index].start))
  //                                         .inHours
  //                                         .toString() +
  //                                     'h',
  //                                 style: TextStyle(
  //                                   color: Colors.black54,
  //                                 ),
  //                               ),
  //                             ],
  //                           ),
  //                         ),
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //                 title: Stack(
  //                   children: <Widget>[
  //                     Row(
  //                       mainAxisSize: MainAxisSize.min,
  //                       children: <Widget>[
  //                         Container(
  //                           margin: EdgeInsets.only(right: 0),
  //                           child: Column(
  //                             mainAxisAlignment: MainAxisAlignment.start,
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             children: <Widget>[
  //                               Padding(
  //                                 padding: const EdgeInsets.only(bottom: 7.0),
  //                                 child: Text(
  //                                   capitalize(entries[index].title),
  //                                   style: TextStyle(
  //                                     color: Colors.black,
  //                                     fontSize: 20,
  //                                   ),
  //                                   textAlign: TextAlign.center,
  //                                 ),
  //                               ),
  //                               Row(
  //                                 mainAxisSize: MainAxisSize.min,
  //                                 children: <Widget>[
  //                                   for (int i = 0;
  //                                       i < entries[index].participants.length;
  //                                       i += 1)
  //                                     i <= 3
  //                                         ? Row(
  //                                             children: <Widget>[
  //                                               Padding(
  //                                                 padding: const EdgeInsets
  //                                                     .symmetric(horizontal: 2),
  //                                                 child: CircleAvatar(
  //                                                   radius: 18,
  //                                                   backgroundColor:
  //                                                       Colors.blue[500],
  //                                                   child: Center(
  //                                                     child: Text(
  //                                                       getInitials(
  //                                                         entries[index]
  //                                                             .participants[i]
  //                                                             .firstName,
  //                                                         entries[index]
  //                                                             .participants[i]
  //                                                             .lastName,
  //                                                       ),
  //                                                       style: TextStyle(
  //                                                           fontWeight:
  //                                                               FontWeight
  //                                                                   .w400),
  //                                                     ),
  //                                                   ),
  //                                                 ),
  //                                               ),
  //                                               if (entries[index]
  //                                                       .participants
  //                                                       .length ==
  //                                                   1)
  //                                                 Padding(
  //                                                   padding:
  //                                                       const EdgeInsets.all(
  //                                                           4.0),
  //                                                   child: Text(
  //                                                     getFullname(
  //                                                       entries[index]
  //                                                           .participants[i]
  //                                                           .firstName,
  //                                                       entries[index]
  //                                                           .participants[i]
  //                                                           .lastName,
  //                                                     ),
  //                                                     style: TextStyle(
  //                                                         fontWeight:
  //                                                             FontWeight.w400),
  //                                                   ),
  //                                                 )
  //                                               else
  //                                                 Container(
  //                                                   width: 0,
  //                                                   height: 1,
  //                                                 ),
  //                                             ],
  //                                           )
  //                                         : Padding(
  //                                             padding:
  //                                                 const EdgeInsets.symmetric(
  //                                                     horizontal: 2),
  //                                             child: CircleAvatar(
  //                                               radius: 18,
  //                                               backgroundColor:
  //                                                   Colors.blue[500],
  //                                               child: Center(
  //                                                 child: Text(
  //                                                   '+${i.toString()}',
  //                                                   style: TextStyle(
  //                                                       fontWeight:
  //                                                           FontWeight.w400),
  //                                                 ),
  //                                               ),
  //                                             ),
  //                                           ),
  //                                 ],
  //                               ),
  //                             ],
  //                           ),
  //                         ),
  //                       ],
  //                     )
  //                   ],
  //                 ),
  //                 subtitle: Padding(
  //                   padding: const EdgeInsets.only(top: 7.0),
  //                   child: Text(
  //                     capitalize(entries[index].description),
  //                     style: TextStyle(color: Colors.black54, fontSize: 16),
  //                   ),
  //                 ),
  //               );
  //             },
  //           )
  //         : ListTile(
  //             dense: true,
  //             title: Text(
  //               'No Events',
  //               style: TextStyle(color: Colors.black54, fontSize: 14),
  //             ),
  //           ),
  //   );
  // }
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      child: entries.isNotEmpty
          ? ListView.builder(
              itemCount: entries.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 16, right: 36),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    DateFormat.Hm().format(DateTime.parse(
                                        '${entries[index].start}')),
                                    style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                    ),
                                  ),
                                  Text(
                                    '1h',
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                              top: 4,
                              left: 62,
                              child: Container(
                                height: 10,
                                width: 10,
                                decoration: BoxDecoration(
                                  color: Colors.red[900],
                                  borderRadius: BorderRadius.circular(100.0),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              '${entries[index].title}',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                for (int i = 0;
                                    i < entries[index].participants.length;
                                    i += 1)
                                  Container(
                                    width: 28,
                                    height: 28,
                                    margin: EdgeInsets.only(right: 4),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.purple[300],
                                      foregroundColor: Colors.white,
                                      child: Text(
                                        getFullname(
                                          entries[index]
                                              .participants[i]
                                              .firstName,
                                          entries[index]
                                              .participants[i]
                                              .lastName,
                                        ),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ),
                                  )
                              ],
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Row(
                              children: <Widget>[
                                Icon(
                                  Icons.location_on,
                                  size: 16,
                                  color: Colors.grey,
                                ),
                                Text(
                                  'Toronto',
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 12),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Divider(),
                  ],
                );
              },
            )
          : ListTile(
              dense: true,
              title: Text(
                'No Events',
                style: TextStyle(color: Colors.black54, fontSize: 14),
              ),
            ),
    );
  }
}
