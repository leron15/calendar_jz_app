import 'package:dio/dio.dart';

import 'package:mobile_new/model/calendar_entry.dart';
import 'package:intl/intl.dart';
import 'package:collection/collection.dart';

class CalendarApiProvider {
  Future<List<CalendarEntry>> getAllCalendarEntries() async {
    var url =
        'http://192.168.0.11:8000/api/schedule/calendars/1/occurrences/?start=2020-01-10&end=2020-03-20';
    try {
      BaseOptions options =
          BaseOptions(receiveTimeout: 2000, connectTimeout: 2000);
      Response response = await Dio(options).get(
        url,
        options: Options(),
      );
      print('Response status: ${response.statusCode}');
      // return CalendarEntry.fromJson(calendarEntryjson) as List;

      return (response.data as List)
          .map((entry) => CalendarEntry.fromJson(entry))
          .toList();
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return [];
    }
  }

  Future<Map<DateTime, List<CalendarEntry>>> getCalendarOccurrences(
      DateTime start, DateTime end) async {
    String startIso = DateFormat('yyyy-MM-dd').format(start).toString();
    String endIso = DateFormat('yyyy-MM-dd').format(end).toString();

    var url =
        'http://192.168.0.11:8000/api/schedule/calendars/1/occurrences/?start=$startIso&end=$endIso';
    print(url);
    try {
      BaseOptions options =
          BaseOptions(receiveTimeout: 2000, connectTimeout: 2000);
      Response response = await Dio(options).get(
        url,
        options: Options(),
      );
      print('Response status: ${response.statusCode}');
      // return CalendarEntry.fromJson(calendarEntryjson) as List;

      var list = (response.data as List)
          .map((entry) => CalendarEntry.fromJson(entry))
          .toList();
      var map = groupBy(list, (entry) {
        var eventDate = DateTime.parse(entry.start);
        return DateTime(eventDate.year, eventDate.month, eventDate.day);
      });
      return map;
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }
}
