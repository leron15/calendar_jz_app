import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart';

class NavigationDrawer extends StatelessWidget {
  // _launchURL(String url) async {
  //   if (await canLaunch(url)) {
  //     await launch(url);
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Image.asset(
                  'assets/images/logo.png',
                  height: 80,
                  fit: BoxFit.contain,
                  alignment: Alignment.topCenter,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: CircleAvatar(
                    backgroundColor: Colors.blue,
                    child: Center(
                      child: IconButton(
                        icon: Text(
                          'd'.toUpperCase(),
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),
                ),
                Text(
                  'Dwayne Johnson',
                  style: TextStyle(color: Colors.black87),
                ),
              ],
            ),
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     image: AssetImage('assets/images/logo.png'),
            //     fit: BoxFit.contain,
            //   ),
            // ),
          ),
          ListTile(
            leading: Icon(Icons.event),
            title: Text('About Schedule'),
          ),
          ListTile(
            leading: Icon(Icons.note_add),
            title: Text('Register'),
          ),
          // ListTile(
          //   leading: Icon(Icons.calendar_today),
          //   title: Text('Calendar'),
          //   onTap: () {
          //     Navigator.pushNamed(context, '/calendar');
          //   },
          // ),
          Divider(),
          ListTile(
            leading: Container(
              width: 32,
              height: 32,
              child: Image.asset('assets/images/facebook.png'),
            ),
            title: Text('Facebook'),
            onTap: () {
              // _launchURL('https://www.facebook.com/ZedojRedTieAndStilettos/');
            },
          ),
          ListTile(
            leading: Container(
              width: 32,
              height: 32,
              child: Image.asset('assets/images/instagram.png'),
            ),
            title: Text('Instagram'),
            onTap: () {
              // _launchURL(
              //     'https://www.instagram.com/zedoj_red_tie_and_stilettos/');
            },
          ),
          ListTile(
            leading: Container(
              width: 32,
              height: 32,
              child: Image.asset('assets/images/linkedin.png'),
            ),
            title: Text('LinkedIn'),
          ),
          ListTile(
            leading: Container(
              width: 32,
              height: 32,
              child: Image.asset('assets/images/twitter.png'),
            ),
            title: Text('Twitter'),
          ),
        ],
      ),
    );
  }
}
